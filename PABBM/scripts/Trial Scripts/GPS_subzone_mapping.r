#install.packages("fpc")
#install.packages("dbscan")

library(factoextra)
library(fpc)
library(dplyr)
library(tidyverse)
library(dbscan)
library(gridExtra)
library(grid)

village_levels <- paste0("E", c(1,seq(3,10,1)))

village_meta <- read_csv("./inputs/meta/project_villages.csv") %>% 
  mutate_if(is.character,funs(factor(.))) %>% 
  mutate(village = fct_relevel(droplevels(village),
                               village_levels))

hunter_responses <- readRDS("./inputs/cartography/hunter_responses.rds") %>% 
  mutate_if(is.character,funs(factor(.)))  %>% 
  # filter(village == "E4" & session > 8 |
  #        village == "E10" & session > 12 |
  #        village %ni% c("E4", "E10") &
  #        village != "E2") %>% 
  mutate(village = fct_relevel(droplevels(village),
                               village_levels))

## reading .rds GPS data
hunts_gps <- readRDS(file = "./inputs/cartography/hunts_gps_aggregate.rds") 
hunts_gps_villages <- hunts_gps %>% left_join(hunter_responses[,c('hunt','local_name_area')], by= "hunt")

#copy 
hunts_gps_backup <- hunts_gps_villages

# filter for a village
hunts_gps_E1 <- hunts_gps_villages %>% 
  filter(village=='E10') %>% 
  dplyr::select(latitude, longitude,local_name_area)


hunts_gps_E1$local_name_area <- as.character(hunts_gps_E1$local_name_area)
hunts_gps_E1[c("local_name_area")][is.na(hunts_gps_E1[c("local_name_area")])] <- "Unknown"
hunts_gps_E1$local_name_area <- as.factor(hunts_gps_E1$local_name_area)
  
ggplot(hunts_gps_E1, aes(x = latitude,
                             y = longitude)) + 
  geom_point(size=4)+
  ggtitle("E10 GPS points")




# plots
ggplot(hunts_gps_E1) +
  geom_point(aes(x=longitude, y=latitude,color=local_name_area),size=1.5) +
  ggtitle("E10 points and named sub-zones")


hunts_gps_E1_sel = hunts_gps_E1%>%
  filter((local_name_area!= 'Unknown'))

ggplot(hunts_gps_E1_sel) +
  geom_point(aes(x=latitude, y=longitude,color=local_name_area),size=1.5)


hunts_gps_E1 %>%
  group_by(local_name_area) %>%
  summarise(n = n())


# from this table we see that Epoutabele has 10 and Zoamagnala has 11 points respectively. 
# We also see from the plot that these zones are in the same area with a lot of unknown points around them.
# this might be an issue
# Let us try to assign the Unknown points to other zones that have bigger points.

# split into test and train
# all unknown points into test
test <- hunts_gps_E1 %>%
  filter(local_name_area=="Unknown") %>%
  select(latitude,longitude)


train_df <- hunts_gps_E1 %>%
  filter(local_name_area!="Unknown") %>%
  select(latitude,longitude,local_name_area)

target_category <- train_df[,3]

target_category <-factor(target_category$local_name_area)

train <- train_df %>%
  select(latitude,longitude)


################### KNN Classification ###############################

##load the package class
library(class)
##run knn function
pr <- knn(train,test,cl=target_category,k=7)

test$local_name_area <- pr

ggplot(test) +
  geom_point(aes(x=latitude, y=longitude,color=local_name_area),size=1.5) 


# append test and train
total_res <- rbind(train_df,test)

ggplot(total_res) +
  geom_point(aes(x=latitude, y=longitude,color=local_name_area),size=1.5) 


villages_list = as.array(unique(hunts_gps_villages$village))
villages_list<-villages_list[villages_list != "E4"]

plots <- vector('list', length(villages_list)*2)
datalist <- list()
i=1
j=1

for (vill in villages_list){
  print(vill)
  # filter for village
  hunts_gps_E1 <- hunts_gps_villages %>% 
    filter(village==vill) %>% 
    dplyr::select(latitude, longitude,local_name_area)
  
  # making local_name_aea ready for classification
  hunts_gps_E1$local_name_area <- as.character(hunts_gps_E1$local_name_area)
  hunts_gps_E1[c("local_name_area")][is.na(hunts_gps_E1[c("local_name_area")])] <- "Unknown"
  hunts_gps_E1$local_name_area <- as.factor(hunts_gps_E1$local_name_area)
  
  # number of zones
  zones<-hunts_gps_E1 %>%
    group_by(local_name_area) %>%
    summarise(n = n())
  
  cat("Number of zones in this village:",nrow(zones)-1,"\n")
  
  # form test train split
  test <- hunts_gps_E1 %>%
    filter(local_name_area=="Unknown") %>%
    select(latitude,longitude)
  
  cat("Number of test points to be classified:",nrow(test),"\n")
  
  
  train_df <- hunts_gps_E1 %>%
    filter(local_name_area!="Unknown") %>%
    select(latitude,longitude,local_name_area)
  
  target_category <- train_df[,3]
  
  target_category <-factor(target_category$local_name_area)
  
  train <- train_df %>%
    select(latitude,longitude)
  
  
  ################### KNN Classification ###############################
  
  ##load the package class
  library(class)
  ##run knn function
  pr <- knn(train,test,cl=target_category,k=7)
  
  test$local_name_area <- pr
  
  # append test and train
  total_res <- rbind(train_df,test)

  total_res$village <- vill
  
  datalist[[i]] <- total_res
  i = i+1
  # plots
  
  
  p1<-ggplot(hunts_gps_E1) +
    geom_point(aes(x=latitude, y=longitude,color=local_name_area),size=1.5)+
    ggtitle(paste(vill,"GPS points-original")) + 
    theme(legend.position="bottom",legend.title= element_blank(),
          legend.text=element_text(size=6))
  plots[[j]] <-p1
  j=j+1
  p2<-ggplot(total_res) +
    geom_point(aes(x=latitude, y=longitude,color=local_name_area),size=1.5)+
    ggtitle(paste(vill,"GPS points-",nrow(test)," predicted")) + 
    theme(legend.position="bottom",legend.title= element_blank(),
          legend.text=element_text(size=6))
  plots[[j]] <-p2
  j=j+1
}


post_class_data = do.call(rbind, datalist)


cowplot::plot_grid(plotlist = plots[3:4], ncol = 2)


################# K-means approach ####################
k_means_df <- hunts_gps_E1[,c(1,2)]
df <- scale(k_means_df)

library(factoextra)
k.res <- kmeans(df, 7, iter.max = 10, nstart = 1)
k.res$centers


fviz_cluster(k.res, data = df,
             #palette = c("#2E9FDF", "#00AFBB", "#E7B800"), 
             geom = "point",
             #ellipse.type = "convex", 
             ggtheme = theme_bw()
)

########### dbscan #############


#dbscan::kNNdistplot(hunts_gps_E1, k =  4)
#abline(h = 0.002, lty = 2)

## DBSCAN clustering
library(cluster)
library(fpc)
library(factoextra)
db <- fpc::dbscan(hunts_gps_E1[,c(1,2)], eps=0.085, MinPts = 5, scale = TRUE,
       method = c("hybrid", "raw", "dist"))

#plot(db, hunts_gps_E1, main = "DBSCAN", frame = FALSE)

#db$cluster
fviz_cluster(db, hunts_gps_E1[,c(1,2)], geom = "point")

hunts_gps_E1$cluster<-db$cluster

library(RColorBrewer)

sp<-ggplot(hunts_gps_E1) +
  geom_point(aes(x=latitude, y=longitude,color=cluster),size=1.5)


sp + scale_color_gradientn(colours = rainbow(21))

centers_db <- hunts_gps_E1 %>%
  filter(cluster != 0) %>%
  group_by(cluster) %>%
  summarize(avg_lat=mean(latitude),
            avg_long= mean(longitude))


hunts_gps_E1<-left_join(hunts_gps_E1, centers_db, by = "cluster")


ggplot(hunts_gps_E1) +
  geom_point(aes(x=latitude, y=longitude,color=cluster),size=1.5) +
 scale_color_gradientn(colours = rainbow(21)) +
  geom_point(aes(x=avg_lat, y=avg_long),size=2.5)


zone_centers <-hunts_gps_E1 %>%
  filter(local_name_area != "Unknown") %>%
  group_by(local_name_area) %>%
  summarize(avg_lat2=mean(latitude),
            avg_long2= mean(longitude))


hunts_gps_E1<-left_join(hunts_gps_E1, zone_centers, by = "local")


############# distance matrix ##########

library(stats)

# filter for a village

vill= "E7"
hunts_gps_E1 <- hunts_gps_villages %>% 
  filter(village==vill) %>% 
  dplyr::select(latitude, longitude,local_name_area)


hunts_gps_E1$local_name_area <- as.character(hunts_gps_E1$local_name_area)
hunts_gps_E1[c("local_name_area")][is.na(hunts_gps_E1[c("local_name_area")])] <- "Unknown"
hunts_gps_E1$local_name_area <- as.factor(hunts_gps_E1$local_name_area)


m<- as.matrix(dist(hunts_gps_E1[,c(1,2)], method = "euclidean", diag = FALSE, upper = FALSE, p = 2))


####### diffusion map
library(diffusionMap)

dmap=diffuse(m, eps.val = epsilonCompute(m), neigen = NULL, t = 0,
        maxdim = 50, delta = 10^-5)


par(mfrow=c(2,1))
plot(t,dmap$X[,1],pch=20,axes=FALSE,xlab="spiral parameter",ylab="1st diffusion coefficient")
box()
plot(1:length(dmap$eigenmult),dmap$eigenmult,typ='h',xlab="diffusion map dimension",ylab="eigen-multipliers")
plot(dmap, las=1)

# if (!requireNamespace("BiocManager", quietly = TRUE))
#   install.packages("BiocManager")
# 
# BiocManager::install("destiny")

library(destiny)
data(guo)

DiffusionMap(m, verbose = FALSE,
             censor_val = 15, censor_range = c(15, 40))

####### heuristics
data <- hunts_gps_E1[,c(1,2,3)]
data$point_idx <- rownames(data)
mean_val <- mean(m)



transform_mat <- NULL
for (i in 1:nrow(data)){
  print(i)
   p <- ifelse(m[i,]<mean_val,data[i,3]$local_name_area,NA)
   transform_mat[[i]] <- unname(p)
}

max_length <- max(unlist(lapply (transform_mat, FUN = length)))
transform_mat<-sapply (transform_mat, function (x) {length (x) <- max_length; return (x)})
transform_mat<-t(transform_mat)


modefunc <- function(x){
  tabresult <- tabulate(x)
  themode <- which(tabresult == max(tabresult))
  if(sum(tabresult == max(tabresult))>1) themode <- NA
  return(themode)
}

#rows
rowwise_modes<-apply(transform_mat, 1, modefunc)
#columns
colwise_modes<-apply(transform_mat, 2, modefunc)

data$rowwise_modes <- rowwise_modes
data$colwise_modes <- colwise_modes

unique(data$local_name_area)
as.numeric(unique(data$local_name_area))
mappings <- as.data.frame(unique(data$local_name_area))
names(mappings)[1] <- "local_name_area_pred"
mappings$codes <- as.numeric(unique(data$local_name_area))

data <- data %>% left_join(mappings, by = c("colwise_modes" = "codes"))

ggplot(data) +
  geom_point(aes(x=latitude, y=longitude,color=local_name_area_pred),size=1.5)+
  ggtitle(paste(vill,"GPS points"," predicted")) + 
  theme(legend.position="bottom",legend.title= element_blank(),
        legend.text=element_text(size=6))


ggplot(data) +
  geom_point(aes(x=latitude, y=longitude,color=local_name_area),size=1.5)+
  ggtitle(paste(vill,"GPS points"," actual")) + 
  theme(legend.position="bottom",legend.title= element_blank(),
        legend.text=element_text(size=6))

unique(data$colwise_modes)

