### load posterior data
load_posteriors = function() {
  ### loads brms posterior data
  
  gmv <<- readRDS("./outputs/model_fits/gmv.rds")
  tmv <<- readRDS("./outputs/model_fits/tmv.rds")
}


### loading empirical data
load_emp_data = function() {
  ### loads actual hunt data
  
  gun_data <<-
    readRDS("./inputs/cartography/derived/bayes_hunts_inputs_gun.rds")
  trap_data <<-
    readRDS("./inputs/cartography/derived/bayes_hunts_inputs_trap.rds")
  village_info <<-
    readRDS("./inputs/cartography/derived/village_info.rds")
  
  
}


### extract gun info
extract_gun_posterior = function() {
  ### extracts the posteriors
  ### for gun
  
  # vars
  b_double_zero <<- as_tibble(posterior_samples(gmv)) %>% pull(b_offtake_d_double_zero_brought)
  b_chevrotine <<- as_tibble(posterior_samples(gmv)) %>% pull(b_offtake_d_chevrotine_brought)
  b_max_distance_gun <<- as_tibble(posterior_samples(gmv)) %>% pull(b_offtake_d_max_km_village)
  
  # intercept
  gun_intercepts <<- as_tibble(posterior_samples(gmv)) %>% select(contains("offtake"),
                                                                 -contains(c("hunter", "cor", "b_", "sd")))
  
  
  
}


### extract trap info
extract_trap_posterior = function() {
  ### same a extract gun
  
  b_traps_days <<- as_tibble(posterior_samples(tmv)) %>% pull(b_offtake_d_traps_days)
  b_traps_checked <<- as_tibble(posterior_samples(tmv)) %>% pull(b_offtake_d_traps_checked)
  b_max_distance_trap <<- as_tibble(posterior_samples(tmv)) %>% pull(b_offtake_d_max_km_village)
  
  trap_intercepts <<- as_tibble(posterior_samples(tmv)) %>% select(contains("offtake"),
                                                                 -contains(c("hunter", "cor", "b_", "sd")))
  
  
}


### ABM run
will_hunt = function(agent, s) {
  ### determines if an agent will
  ### hunt at this time step
  
  # probability the person hunts
  agent$does_hunt = rbinom(n = 1, size = 1, p = as.numeric(agent$p_hunt))
  agent$step = s
  return(agent)
}

allowed_hunt = function(agent, h_tracker) {
  ### checks the tracking table to 
  ### see if a hunter is allowed to 
  ### hunt
  
  # get info
  id = as.numeric(agent$id)
  
  bool_1 = dim(h_tracker)[1] == 0
  
  # if trackers are both empty, or agent not in trackers
  if((bool_1 == T) | (id %in% h_tracker$id == F)) {
    return(agent)
  }
  
  row_num_h = which(h_tracker$id == id)
  
  h_allow = h_tracker[row_num_h, "allowed_hunt"]
  
  # if hunt or offtake is maxed, retire
  if(h_allow != 1) {
    agent$does_hunt = 0
  }
  return(agent)
}

update_off_days = function(agent, h_tracker, days) {
  ### need to update the days
  ### for hunters who did not
  ### hunt, so they can be reset
  
  # get hunter id
  id = as.numeric(agent$id)
  row_num = which(h_tracker$id == id)
  
  # if h_tracker is empty, or agent not in tracker
  if (dim(h_tracker)[1] == 0  | id %in% h_tracker$id == F) {
    return(h_tracker)
  }
  
  # still update the day and week
  else{
    h_tracker[row_num, "curr_day"]  = days
    h_tracker[row_num, "week"]  = floor(days / 7) + 1
    
    # reset the count_this_week
    if (mod(days, 7) == 0) {
      h_tracker[row_num, "count_this_week"] = 0
      h_tracker[row_num, "allowed_hunt"] = 1
    }
  }
  return(h_tracker)
}

z_score = function(x, vec) {
  ### calculates z-score
  
  return((x - mean(vec, na.rm = T)) / sd(vec, na.rm = T))
}

does_gun = function(agent, gun_dataset) {
  ### calculates offtake of a
  ### gun hunter
  
  # subset intercept and data by village
  v_id = as.character(agent[1, "village"])
  
  
  g_data = subset(gun_dataset, village == as.character(v_id))
  g_inter = pull(gun_intercepts[, v_id])
  
  
  if (agent$hunt_method == "gun" || agent$curr_method == "gun") {
    # sample max_km_gun
    gun_dist = g_data[g_data$guntrap == "gun", ]
    
    agent$max_km = sample(na.omit(gun_dist$max_km_village), 1)
    agent$max_km_std = z_score(agent$max_km, gun_dist$max_km_village)
  }
  
  # sample total_double_zero
  agent$total_double_zero = sample(na.omit(g_data$double_zero_before), 1)
  agent$total_double_zero_std = z_score(agent$total_double_zero, g_data$double_zero_before)
  
  # sample total_chevrotine
  agent$total_chevrotine = sample(na.omit(g_data$chevrotine_before), 1)
  agent$total_chevrotine_std = z_score(agent$total_chevrotine, g_data$chevrotine_before)
  
  # fixing NaN
  agent[is.na(agent)] = 0
  
  # calculate offtake
  agent$offtake_gun = exp(
    sample(na.omit(g_inter), 1) +
      sample(b_max_distance_gun, 1) * agent$max_km_std +
      sample(b_double_zero, 1) * agent$total_double_zero_std +
      sample(b_chevrotine, 1) * agent$total_chevrotine_std
  )
  
  
  if (agent$curr_method == "both") {
    return(agent)
  }
  
  # add current method
  agent$curr_method = "gun"
  agent$total_offtake = agent$offtake_gun
  
  return(agent)
}

does_trap = function(agent, trap_dataset) {
  ### calculates offtake of a
  ### trap hunter
  
  # subset intercept and data by village
  v_id = as.character(agent[1, "village"])
  
  t_data = subset(trap_data, village == as.character(v_id))
  t_inter = pull(trap_intercepts[, v_id])
  
  
  if (agent$hunt_method == "trap" || agent$curr_method == "trap") {
    # sample max_km_trap
    trap_dist = t_data[t_data$guntrap == "trap", ]
    
    agent$max_km = sample(na.omit(trap_dist$max_km_village), 1)
    agent$max_km_std = z_score(agent$max_km, trap_dist$max_km_village)
    #agent$max_km_traps = sample(na.omit(trap_dist$max_km_village), 1)
    #agent$max_km_traps_std = z_score(agent$max_km_traps, trap_dist$max_km_village)
  }
  
  # sample traps_checked
  agent$traps_checked = sample(na.omit(t_data$traps_checked), 1)
  agent$traps_checked_std = z_score(agent$traps_checked, t_data$traps_checked)
  
  
  # sample trap_days
  agent$traps_days = sample(na.omit(t_data$traps_days), 1)
  agent$traps_days_std = z_score(agent$traps_days, t_data$traps_days)
  
  # fixing NaN
  agent[is.na(agent)] = 0
  
  # calculate offtake
  agent$offtake_traps = exp(
    sample(na.omit(t_inter), 1) +
      sample(b_traps_checked, 1) * agent$traps_checked_std +
      sample(b_max_distance_trap, 1) * agent$max_km_std +
      sample(b_traps_days, 1) * agent$traps_days_std
  )
  
  if (agent$curr_method == "both") {
    return(agent)
  }
  
  # add current method
  agent$curr_method = "trap"
  agent$total_offtake = agent$offtake_traps
  
  return(agent)
}

both_decision = function(g, t, b) {
  ### samples probabilities of
  ### hunting methods for the
  ### both hunter
  
  s = sample(c(rep("gun", g), rep("trap", t), rep("both", b)), 1)
  return(s)
}

does_both = function(agent, gun_dataset, trap_dataset) {
  ### calculates offtake of a
  ### hunter who does both, or
  ### sends them to only gun /
  ### trap hunt
  
  # get prob values of hunt method
  p_g = round(as.double(agent$p_gun) * 100, 0)
  p_t = round(as.double(agent$p_trap) * 100, 0)
  p_b = round(as.double(agent$p_both) * 100, 0)
  
  # returns a decision for hunt method
  method = both_decision(p_g, p_t, p_b)
  
  if (method == "gun") {
    agent$curr_method = "gun"
    agent = does_gun(agent, gun_dataset)
    return(agent)
  }
  
  if (method == "trap") {
    agent$curr_method = "trap"
    agent = does_trap(agent, trap_dataset)
    return(agent)
  }
  
  if (method == "both") {
    # add current method
    agent$curr_method = "both"
    
    v_id = as.character(agent[1, "village"])
    b_data = rbind(gun_data, trap_data)
    b_data = subset(b_data, village == as.character(v_id))
    
    # calculate distance
    both_dist = b_data[b_data$guntrap == "both",]
    agent$max_km = sample(na.omit(both_dist$max_km_village), 1)
    agent$max_km_std = z_score(agent$max_km, both_dist$max_km_village)
    
    # add current method
    agent$curr_method = "both"
    
    # sends agent to both gun and trap
    agent = does_trap(does_gun(agent, gun_dataset), trap_dataset)
    
    # total offtake
    offtake_subset = agent %>% select(contains("offtake"))
    new_offtake = as.numeric(rowSums(offtake_subset))
    agent$total_offtake = new_offtake
    
    return(agent)
  }
}

fix_inter_names = function(data) {
  ### cleans intercept names
  ### from brms output
  
  # regex is the worst
  p = "[A-Za-z]+[:digit:]+"
  
  # cleans names
  for (i in seq(1, length(data), 1)) {
    c_name = colnames(gun_intercepts)[i]
    new_name = str_extract(c_name , p)
    colnames(data)[i] = new_name
  }
  
  return(data)
}

null_to_zero = function(df_hunters) {
  ### makes null frames into 0 x 0
  ### df
  
  if(is.null(df_hunters) == T) {
    z = data.frame()
    return(z)
  }
  
  return(df_hunters)
}

combine = function(g_hunters, t_hunters, b_hunters) {
  ### combines multiple hunter
  ### data frames into single
  ### output
  
  # trap and gun missing
  if (nrow(t_hunters) == 0 & nrow(g_hunters) == 0) {
    print("g and t are missing")
    return(b_hunters)
  }
  
  # both and trap missing
  if (nrow(b_hunters) == 0 & nrow(t_hunters) == 0) {
    print("b and t are missing")
    return(g_hunters)
  }
  
  # both missing
  if (nrow(b_hunters) == 0) {
    print("b is missing")
    a_hunters = full_join(g_hunters, t_hunters)
    return(a_hunters)
  }
  
  # trap missing
  if (nrow(t_hunters) == 0) {
    print("t is missing")
    a_hunters = full_join(g_hunters, b_hunters)
    return(a_hunters)
  }
  
  # gun missing
  if (nrow(g_hunters) == 0) {
    print("g is missing")
    a_hunters = full_join(t_hunters, b_hunters)
    return(a_hunters)
  }
  
  # combine all full frames
  a_hunters = full_join(g_hunters, t_hunters)
  a_hunters = rbind(a_hunters, b_hunters)
  
  return(a_hunters)
  
}

collect_hunt = function(agent, tracker, days, max_val) {
  ### stores the hunt instance 
  ### of each hunter and
  ### updates after every
  ### hunt
  
  # get hunter id
  id = as.numeric(agent$id)
  row_num = which(tracker$id == id)
  
  # if tracker is empty or a new hunter has appeared
  if(dim(tracker)[1] == 0 | id %in% tracker$id == F) {
    new_instance = data.frame('id' = id, 'total_count' = 1, 'count_this_week' = 1, 
                              'curr_day' = days, 'week' = floor(days/7) + 1, 'allowed_hunt' = 1)
    tracker = rbind(tracker, new_instance)
  }
  
  # a hunter id already exists
  else {
    row_num = which(tracker$id == id)
    tracker[row_num, "total_count"]  = tracker[row_num, "total_count"] + 1
    tracker[row_num, "count_this_week"]  = tracker[row_num, "count_this_week"] + 1
    tracker[row_num, "curr_day"]  = days
    tracker[row_num, "week"]  = floor(days/7) + 1
    the_week = tracker[row_num, "week"]
    
    # reset the count_this_week
    if(mod(days, 7) == 0) {
      tracker[row_num, "count_this_week"] = 0
      tracker[row_num, "allowed_hunt"] = 1
    }
    
    # if agent goes over week max, then they cant hunt
    if((tracker[row_num, "count_this_week"]) >= max_val) {
      tracker[row_num, "allowed_hunt"] = 0
    }
  }
  return(tracker)
}

collect_offtake = function(agent, tracker, max_val) {
  ### stores the offtake 
  ### of each hunter and
  ### updates after every
  ### hunt
  
  id = as.numeric(agent$id)
  offtake_subset = agent %>% select(contains("offtake"))
  new_offtake = as.numeric(rowSums(offtake_subset))
  
  # if tracker is empty or a new hunter has appeared
  if(dim(tracker)[1] == 0 | id %in% tracker$id == F) {
    new_instance = data.frame('id' = id, 'total_offtake' = new_offtake, 'allowed_hunt' = 1)
    tracker = rbind(tracker, new_instance)
  }
  
  # a hunter id already exists
  else {
    row_num = which(tracker$id == id)
    tracker[row_num, "total_offtake"]  = tracker[row_num, "total_offtake"] + new_offtake
    
    # if hunter goes over limit, then cant hunt
    if(tracker[row_num, "total_offtake"] >= max_val) {
      tracker[row_num, "allowed_hunt"] = 0
    }
     
  }
  return(tracker)
}

is_max_hunt = function(agent, tracker, max_val) {
  ### each hunter is checked
  ### to see if they have not
  ### maxed out their hunts 
  ### per week
  
  # if agent doesnt want to hunt, exit now
  if(agent$does_hunt == 0) {
    return(agent)
  }
  
  id = as.numeric(agent$id)
  
  if(id %in% tracker$id == T) {
    row_num = which(tracker$id == id)
    the_week = tracker[row_num, "week"]
    
    if((tracker[row_num, "count_this_week"]) >= max_val) {
      agent$does_hunt = 0
      print("ive maxed hunted for the week")
    }
  }
  return(agent)
}

is_max_offtake = function(agent, tracker, max_off) {
  ### each hunter is checked
  ### to see if they have hit 
  ### the max number of offtake
  ### for a given week
  
  # if agent doesnt want to hunt, exit now
  if(agent$does_hunt == 0) {
    return(agent)
  }
  
  id = as.numeric(agent$id)
  
  # checks to see if hunter has maxed offtake for the week
  if(id %in% tracker$id == T) {
    row_num = which(tracker$id == id)
    
    if((tracker[row_num, "total_offtake"]) >= max_off) {
      agent$does_hunt = 0
      print("ive maxed offtake")
    }
  }
  return(agent)
}

max_offtake_per_hunt = function(agent, max_val) {
  ### sets the offtake
  ### of the hunt to the
  ### max no. per hunt
  

  
  if(as.numeric(agent$total_offtake) > max_val) {
    agent$total_offtake = max_val
    print("maxed")
  }
  
  return(agent)
}


### analysis
round_and_floor = function(hunter_df) {
  ### gets round and
  ### floor of offtake
  
  
  # get offtake vector
  offtake = hunter_df %>%
    select(contains("offtake")) %>%
    pull()
  
  
  if (any(unique(hunter_df$curr_method) == "gun")) {
    hunter_df$offtake_round = round(offtake)
    hunter_df$offtake_floor = floor(offtake)
  }
  
  if (any(unique(hunter_df$curr_method) == "trap")) {
    hunter_df$offtake_traps_round = round(offtake)
    hunter_df$offtake_traps_floor = floor(offtake)
  }
  
  return(hunter_df)
  
}

gun_data_collector = function(g_data, v) {
  ### collects relevant empirical
  ### data from gun data
  
  g_data_tmp <- g_data %>%
    filter(village == v) %>%
    select(
      hunter,
      max_km_village,
      double_zero_brought,
      chevrotine_brought,
      max_km_village,
      offtake
    )
  
  return(g_data_tmp)
}

trap_data_collector = function(t_data, v) {
  ### collects relevant empirical
  ### data from gun data
  
  t_data_tmp <- t_data %>%
    filter(village == v) %>%
    select(
      hunter,
      max_km_village,
      traps_checked,
      traps_days,
      offtake
    )
  
  return(t_data_tmp)
  
}

both_select_vars = function(b_hunters, df_hunters) {
  n = colnames(df_hunters)
  b_hunters = b_hunters %>% select(n)
  return(b_hunters)
}

fix_gun_names = function(g_hunters) {
  ### fixes the gun hunter
  ### names
  
  g_hunters %<>%
    rename("max_km_village" = "max_km",
           "double_zero_brought" ="total_double_zero",
           "chevrotine_brought" = "total_chevrotine",
           "offtake" = "offtake_gun")
  
  return(g_hunters)
}

fix_trap_names = function(t_hunters) {
  ### fixes the gun hunter
  ### names
  
  t_hunters %<>%
    rename("max_km_village" = "max_km",
           "traps_checked" = "traps_checked",
           "traps_days" = "traps_days",
           "offtake" = "offtake_traps",
           "offtake_round" = "offtake_traps_round",
           "offtake_floor" = "offtake_traps_floor")
  
  return(t_hunters)
}

drop_col_na = function(df_hunters) {
  
  ### drops columns that have
  ### all NA
  
  df_drop = df_hunters[colSums(!is.na(df_hunters)) > 0]
  return(df_drop)
}

clean_summary_table = function(df) {
  ### cleans the output
  
  df = as.data.frame(t(df))
  
  names(df)<- df[1,]
  df = df[-1,]
  
  return(df)
}

make_summary_table = function(ss) {
  ### makes the summary
  ### table
  
  metrics = vector()
  values = vector()
  
  # goes through the rows
  for (i in seq(1, nrow(ss), 1)) {
    #goes through the cols
    for (j in seq(2 , ncol(ss), 1)) {
      # concats the names
      m_name = as.character(ss[i, "key"] %>% pull())
      s_name = names(ss[j])
      c_name = paste(s_name, m_name, sep = "_")
      num = ss[i, j] %>% pull()
      
      # append
      metrics = append(metrics, c_name)
      values = append(values, num)
      
      # cbind
      d = cbind(metrics, values)
    }
  }
  
  
  
  calc_table = clean_summary_table(d)
  return(calc_table)
  
  
}

calc_summary_table = function(df_hunters, type) {
  ### caclulates the summ.
  ### stats of a df
  
  ss = gather(df_hunters, factor_key = T)
  
  ss = ss  %>% group_by(key) %>%
    summarise(
      mean = mean(value, na.rm = T),
      min = min(value, na.rm = T),
      max = max(value, na.rm = T),
      sd = sd(value, na.rm = T),
      .groups = "drop"
    )
  
  if (type == "actual") {
    # duplicate row
    row_c = ss[nrow(ss), ]
    row_c$key = "offtake_floor"
    
    # rename old row
    ss$key = gsub("offtake", "offtake_round", ss$key)
    
    #rbind
    ss = rbind(ss, row_c)
  }
  s_table = make_summary_table(ss)
  return(s_table)
}

hunt_summary = function(df_hunters, village, type) {
  ### summarizes values for
  ### a hunt type
  
  if(type == "pred") {
    df_hunters = df_hunters %>% 
      select(id, curr_method, max_km_village, contains(c("_brought", "_gun_", "_checked", "_days" , "_round", "_floor")), 
             -contains("std"))
    n_hunt = nrow(df_hunters)
    n_hunter = length(unique(df_hunters$id))
    c_method = unique(df_hunters$curr_method)[-2]
    df_hunters$id = NULL
    df_hunters$curr_method = NULL 
  }
  
  else{
    # getting hunt info
    n_hunt = nrow(df_hunters)
    n_hunter = length(unique(df_hunters$hunter))
    df_hunters$hunter = NULL
    c_method = "none"
  }
  
  df_final = calc_summary_table(df_hunters, type)
  
  df_final$n_hunt = n_hunt
  df_final$n_hunter = n_hunter
  df_final$village = as.character(village)
  
  if(type == "pred" & c_method == "gun") {
    df_final$prop_zero_rounded = nrow(df_hunters[df_hunters$offtake_round == 0, ]) /
      nrow(df_hunters)
    df_final$prop_zero_floor = nrow(df_hunters[df_hunters$offtake_round == 0, ]) /
      nrow(df_hunters)
  }
  
  if(type == "pred" & c_method == "trap") {
    df_final$prop_zero_rounded = nrow(df_hunters[df_hunters$offtake_round == 0, ]) /
      nrow(df_hunters)
    df_final$prop_zero_floor = nrow(df_hunters[df_hunters$offtake_round == 0, ]) /
      nrow(df_hunters)
  }
  
  if(type == "actual" & c_method == "none"){
    df_final$prop_zero_rounded = nrow(df_hunters[df_hunters$offtake == 0, ]) /
      nrow(df_hunters)
    df_final$prop_zero_floor = nrow(df_hunters[df_hunters$offtake == 0, ]) /
      nrow(df_hunters)
  }
  return(df_final)
}

diff_function = function(actual, pred) {
  
  v = as.character(actual$village)
  actual$village = NULL
  pred$village = NULL
  
  row = nrow(actual)
  col = ncol(actual)
  df = data.frame(matrix(nrow = row, ncol = col))
  
  
  names(df) = names(actual)
  
  for(i in seq(1, ncol(actual), 1)) {
    df[1,i] = as.numeric(actual[1,i]) - as.numeric(pred[1,i])
  }
  
  df$village = v
  
  return(df)
}