**Project Background**

Bushmeat hunting is a prevalent activity in many communities across tropics. It is vital for the food, financial, and cultural security of local people across the tropics. 
However, due to the commercialization of unsustainable and excessive hunting practices, the biodiversity and human well-being in these communities is threatened. 
This is the case in Gabon, a country in central Africa.

Hence, the Nsombou Abalghe Dzal Project focuses on encouraging local participation in wildlife management. In this project, we created a prototype of an analytical framework that enables
Gabonese villages to make more sustainable hunting decisions by predicting the consequences of their management decisions and adapting accordingly.
We also demonstrate a prototype of an RShiny application with a preliminary working code repository for the modelling framework, results obtained, caveats and future work. This application will be used by the Gabonese community to
simulate and predict the outcomes of their hunting policy decisions.

**Description**

This the working file repository for the MIDS work done for the project Nsombou Abalghe-Dzal: participatory agent-based Bayesian modeling (PABBM) for community adaptive bushmeat hunting management by Gabonese villages.

* Project Timeline: Aug 2020 to April 2021
* Team Members: Abhishek Baral and Srishti Saha
* Mentor: Graden Froese

**Repository structure**

- [PABBM][link0] : contains scripts, inputs and output files for production of working code, tools, and analyses.
- [reports_and_presenations][link1] : has final paper and presentation
- [Rshiny_simulation_app][link2] : has scripts and necessary files to run the Rshiny app
 
[link0]: https://gitlab.oit.duke.edu/duke-mids/workingprojectrepositories/2020-2021/community-bushmeat-hunting/-/tree/master/PABBM
[link1]: https://gitlab.oit.duke.edu/mids-capstone-projects/2020-2021/participatory-forecasting-of-community-hunting-management/-/tree/master/reports_and_presentations
[link2]: https://gitlab.oit.duke.edu/mids-capstone-projects/2020-2021/participatory-forecasting-of-community-hunting-management/-/tree/master/Rshiny_simulation_app



*Please Note*
This is a WIP participatory forecasting tool, which is being continuously updated. If intersted in final products in the future please go to the forthcoming website [nadagabon.org](https://nadagabon.org)

