### define UI
ui = fluidPage(# application title
  titlePanel("NADA Project"),
  
  mainPanel(
    tabsetPanel(
      type = "tabs",
      
      # welcome tab
      tabPanel(
        title = "Welcome",
        
        # display image
        img(
          src = 'picture.png',
          height = '400px',
          width = '400px',
          align = "right"
        ),
        
        # display info text
        h5(
          "Welcome to the PABBM application. Please input the village you would like to analyze"
        ),
        
        # select village
        selectInput(
          inputId = "villages_list",
          label = "Choose village",
          choices = village_options
        ),
        
      ),
      
      
      
      
      
      
      ### baseline tab
      tabPanel(
        title = "Baseline",
        
        sidebarPanel(
          # input number of days
          numericInput(
            inputId = "ndays",
            label = "Choose number of days",
            min = 0,
            max = 365,
            value = 10
          ),
          
          # calculate button for baseline
          actionButton(inputId = "go",
                       label = "Execute"),
        ),
        
        
        # print simulation
        #tableOutput("results"),
        
        mainPanel(width = 8,
                  fluidRow(
                    splitLayout(
                      cellWidths = c("50%", "50%"),
                      
                      # column chart of offtake
                      plotOutput("offtake_col"),
                      
                      # column chart of cnt of huntmethod
                      plotOutput("offtake_curr_method")
                    )
                  ),
                  
                  # density chart of offtake
                  plotOutput("offtake_dist")),
      ),
      
      
      
      
      
      
      
      
      ### adjusted tab
      tabPanel(
        title = "Adjusted tabs",
        
        
        
        fluidRow(
          # hunter adjustment
          sidebarPanel(
            width = 11,
            # input number of gun hunters
            fluidRow(column(
              width = 4,
              numericInput(
                inputId = "n_gun_hunters",
                label = "Choose number of gun hunters",
                min = 0,
                max = 100,
                value = 2
              ),
            ),
            
            # display actual number gun hunters
            column(
              width = 3,
              # print actual num gun hunters
              textOutput("num_gun_hunters_act")
            )),
            
            # input gun parameters
            conditionalPanel(condition = "input.n_gun_hunters > 0",
                             fluidRow(width = 4,
                                      
                                      # p_hunt
                                      column(
                                        width = 3,
                                        numericInput(
                                          inputId = "p_gun_hunt",
                                          label = "Choose average number of days to gun hunt",
                                          min = 1,
                                          max = 7,
                                          value = 3
                                        )
                                      ), ),),
            
            # input number of trap hunters
            fluidRow(column(
              width = 4,
              numericInput(
                inputId = "n_trap_hunters",
                label = "Choose number of trap hunters",
                min = 0,
                max = 100,
                value = 2
              ),
            ),
            
            # display actual number trap hunters
            column(
              width = 3,
              # print actual num trap hunters
              textOutput("num_trap_hunters_act")
            )),
            
            # input trap parameters
            conditionalPanel(condition = "input.n_trap_hunters > 0",
                             fluidRow(width = 4,
                                      
                                      # p_hunt
                                      column(
                                        width = 3,
                                        numericInput(
                                          inputId = "p_trap_hunt",
                                          label = "Choose average number of days to trap hunt",
                                          min = 1,
                                          max = 7,
                                          value = 4
                                        )
                                      ), ),),
            
            # input number of both hunters
            fluidRow(column(
              width = 4,
              numericInput(
                inputId = "n_both_hunters",
                label = "Choose number of both hunters",
                min = 0,
                max = 10,
                value = 2
              ),
            ),
            
            # display actual number both hunters
            column(
              width = 3,
              # print actual num both hunters
              textOutput("num_both_hunters_act")
            )),
            
            # input both parameters
            conditionalPanel(condition = "input.n_both_hunters > 0",
                             fluidRow(
                               # p_hunt
                               column(
                                 width = 3,
                                 numericInput(
                                   inputId = "p_both_hunt",
                                   label = "Choose average number of days to both hunt",
                                   min = 1,
                                   max = 7,
                                   value = 3
                                 ),
                               ),
                               
                               # p_gun
                               column(
                                 width = 3,
                                 numericInput(
                                   inputId = "p_both_gun",
                                   label = "Choose probability of using a gun",
                                   min = 0,
                                   max = 1,
                                   value = .3
                                 ),
                               ),
                               
                               # p_trap
                               column(
                                 width = 3,
                                 numericInput(
                                   inputId = "p_both_trap",
                                   label = "Choose probability of using a trap",
                                   min = 0,
                                   max = 1,
                                   value = .3
                                 ),
                               ),
                               
                               # p_both
                               column(
                                 width = 3,
                                 numericInput(
                                   inputId = "p_both_both",
                                   label = "Choose probability of using both types",
                                   min = 0,
                                   max = 1,
                                   value = .3
                                 )
                               )
                             ),),
          ),
          
          
          # rules adjustment
          sidebarPanel(
            width = 4,
            
            # close subzone
            checkboxGroupInput(
              inputId = "subzone",
              label = "Choose which subzone to close",
              choices = c("option1", "option2")
            ),
            
            # limit hunts per week
            numericInput(
              inputId = "max_hunt",
              label = "Max number of hunts per week",
              min = 1,
              max = 100,
              value = NA
            ),
            # think how min and max work with ABM
            
            # limit 00
            numericInput(
              inputId = "max_00",
              label = "Max number of 00",
              min = 0,
              max = 100,
              value = NA
            ),
            
            # limit chevrotine
            numericInput(
              inputId = "max_chevrotine",
              label = "Max number of chevrotine",
              min = 0,
              max = 100,
              value = NA
            ),
            
            # limit traps
            numericInput(
              inputId = "max_traps",
              label = "Max number of traps",
              min = 0,
              max = 100,
              value = NA
            ),
            
            # limit max_km_gun
            numericInput(
              inputId = "max_km_gun",
              label = "Max number of km for gun",
              min = 0,
              max = 100,
              value = NA
            ),
            
            # limit max_km_traps
            numericInput(
              inputId = "max_km_traps",
              label = "Max number of km for traps",
              min = 0,
              max = 100,
              value = NA
            ),
            
            # limit offtake
            numericInput(
              inputId = "max_offtake",
              label = "Max number of offtake",
              min = 0,
              max = 100,
              value = NA
            ),
            
          ),
          
          
        ),
        
        # build button for applying rules
        actionButton(inputId = "apply",
                     label = "Apply"),
        
        mainPanel(width = 8,
                  fluidRow(
                    splitLayout(
                      cellWidths = c("50%", "50%"),
                      
                      # column chart of offtake
                      plotOutput("offtake_col_adj"),
                      
                      # column chart of cnt of huntmethod
                      plotOutput("offtake_curr_method_adj")
                    )
                  ),
                  
                  # density chart of offtake
                  plotOutput("offtake_dist_adj")
                ),
      ),
      
      
      
      
      # compare tab
      tabPanel("Compare", verbatimTextOutput("Adjusted"))
    ),
  ))
